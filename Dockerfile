FROM nginx:1.17

RUN apt-get update && apt-get install -y openssl

RUN mkdir -p /etc/ssl/private
RUN mkdir -p /etc/ssl/certs

COPY nginx.conf /etc/nginx/nginx.conf
RUN rm /etc/nginx/conf.d/default.conf
RUN mkdir /ssl_conf
COPY localhost.conf /ssl_conf/localhost.conf
